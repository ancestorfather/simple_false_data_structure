#include<bits/stdc++.h>
using namespace std;
int m,n,a[1000010],len;
bool flag=true;
int read()
{
	int s=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		s=s*10+ch-'0';
		ch=getchar();
	}
	return s*f;
}
struct node
{
	int l,r;
	int used,add;
}q[4*1000010];
void build(int p,int l,int r)
{
	q[p].l=l,q[p].r=r;
	if(l==r)
	{
		q[p].used=read();
		return;
	}
	int mid=(l+r)/2;
	build(p*2,l,mid);
	build(p*2+1,mid+1,r);
	q[p].used=min(q[p*2].used,q[p*2+1].used);	
}
void spread(int p)
{
	q[p*2].used-=q[p].add;
	q[p*2+1].used-=q[p].add;
	q[p*2].add+=q[p].add;
	q[p*2+1].add+=q[p].add;
	q[p].add=0;
	return;
}
void change(int p,int l,int r,int d)
{
	if(l<=q[p].l&&r>=q[p].r)
	{
		q[p].used-=d;
		q[p].add+=d;
		return;
	}
	if(q[p].add) spread(p);
	int mid=(q[p].l+q[p].r)/2;
	if(l<=mid) change(p*2,l,r,d);
	if(r>mid) change(p*2+1,l,r,d);
	q[p].used=min(q[p*2].used,q[p*2+1].used);
}
int ask(int p,int l,int r)
{
	if(l<=q[p].l&&r>=q[p].r) 
		return q[p].used;
	int mid=(q[p].l+q[p].r)/2;
	int val=(1<<30);
	if(l<=mid) val=min(val,ask(p*2,l,r));
	if(r>mid) val=min(val,ask(p*2+1,l,r));
	return val;
}
int main()
{
	n=read(),m=read();
	build(1,1,n);
	for(register int i=1; i<=m; ++i)
	{
		int l,r,num;
		num=read(),l=read(),r=read();
		if(!flag) continue;
		if(ask(1,l,r)<num) flag=false,len=i;
		else change(1,l,r,num);
		
	}
	if(len==0) cout<<0<<endl;
	else cout<<-1<<endl<<len<<endl;
	return 0;
}
