#include<bits/stdc++.h>
using namespace std;
int read()
{
	int s=0,f=1;
	char ch=getchar();
	while(ch<'0'&&ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		s=s*10+ch-'0';
		ch=getchar();
	}
	return f*s;
}
struct edge
{
	int u,v,next;
}q[50010];
int tot,n,x,y,head[50010],ans[50010],zans;
void addedge(int x,int y)
{
	q[++tot].u=x,q[tot].v=y,q[tot].next=head[x],head[x]=tot;
}
void tj(int x)
{
	for(int i=head[x]; i; i=q[i].next) ++ans[x];
}
int main()
{
	n=read();
	for(int i=1; i<n; ++i) x=read(),y=read(),addedge(x,y),addedge(y,x);
	for(int i=1; i<=n; ++i) tj(i),zans=max(zans,ans[i]);
	for(int i=1; i<=n; ++i) if(ans[i]==zans) cout<<i<<" ";
	return 0;
}
