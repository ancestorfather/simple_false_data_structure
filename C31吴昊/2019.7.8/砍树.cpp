#include<bits/stdc++.h>
using namespace std;
int n,m;
double a[200010];
double xx=3.14;
int read()
{
	int s=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		s=s*10+ch-'0';
		ch=getchar();
	}
	return s*f;
}
struct node
{
	int l,r;
	int sum;
}q[4*200010];
void build(int p,int l,int r)
{
	q[p].l=l,q[p].r=r;
	if(l==r)
	{
		q[p].sum=read();
		return;
	}
	int mid=(l+r)/2;
	build(p*2,l,mid);
	build(p*2+1,mid+1,r);
	q[p].sum=q[p*2].sum+q[p*2+1].sum;
}
double ask(int p,int l,int r)
{
	if(l<=q[p].l&&r>=q[p].r) 
		return q[p].sum;
	int mid=(q[p].l+q[p].r)/2;
	double val=0;
	if(l<=mid) val+=ask(p*2,l,r);
	if(r>mid) val+=ask(p*2+1,l,r);
	return val;
}
void change(int p,int x)
{
	if(q[p].l==q[p].r)
	{
		q[p].sum=0;
		return;
	}
	int mid=(q[p].l+q[p].r)/2;
	if(x<=mid) change(p*2,x);
	else change(p*2+1,x);
	q[p].sum=q[p*2].sum+q[p*2+1].sum;
}
int main()
{
	n=read();
	build(1,1,n);
	m=read();
	for(int i=1; i<=m; ++i)
	{
		int l,r;
		l=read(),r=read();
		a[i]=ask(1,l,r)*xx;
		change(1,(l+r)/2);
	}
	for(int i=1; i<=m; ++i) cout<<fixed<<setprecision(2)<<a[i]<<endl;
	return 0;
}
