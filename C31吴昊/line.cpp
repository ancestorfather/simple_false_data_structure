#include<bits/stdc++.h>
using namespace std;
int n,m;
int gcd(int a,int b)
{
	while(b!=0)
	{
		int d=a%b;
		a=b,b=d;
	}
	return a;
}
int main()
{
	freopen("line.in","r",stdin);
	freopen("line.out","w",stdout);
	cin>>n>>m;
	if(n>m) swap(n,m);
	if(n==m) cout<<"1/1";
	else
	{
		int d=gcd(n,m);
		n/=d,m/=d;
		if(n>m) swap(m,n);
		if(n%2==0||m%2==0) cout<<"1/2"<<endl;
		else cout<<n*(m-1)/2+(n+1)/2<<"/"<<m*n<<endl;
	}
	return 0;
}
