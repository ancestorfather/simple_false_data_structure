#include<bits/stdc++.h>
using namespace std;
int read()
{
	int s=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		s=s*10+ch-'0';
		ch=getchar();
	}
	return f*s;
}
struct edge
{
	int u,v,next,w;
}q[1010];
set<int> a;
int n,m,k,tot,x,y,head[1010],dd=(1<<30),nm=(1<<30);
void addedge(int x,int y)
{
	q[++tot].u=x,q[tot].v=y,q[tot].next=head[x],head[x]=tot,q[tot].w=1;
	cout<<a.find(y)<<endl;
}
void dfs(int x)
{
	if(!head[x])
	{
		if(m>=0) dd=min(dd,x);
		else nm=min(nm,x);
		return;
	}
	for(int i=head[x]; i; i=q[i].next)
		m-=q[i].w,dfs(q[i].v),m+=q[i].w;
	return;
}
int main()
{
	n=read(),m=read(),k=read();
	if(k) for(int i=1; i<=k; ++i) x=read(),a.insert(x);
	for(int i=1; i<n; ++i)
	{
		x=read(),y=read();
		addedge(x,y);
	}
//	cout<<n<<endl;
//	for(int i=1; i<n; ++i) cout<<q[i].u<<" "<<q[i].v<<" "<<q[i].next<<endl;
	dfs(1);
	cout<<nm<<endl<<dd<<endl;
	return 0;
}
