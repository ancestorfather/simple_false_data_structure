#include<bits/stdc++.h>
using namespace std;
int read()
{
	int s=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		s=s*10+ch-'0';
		ch=getchar();
	}
	return f*s;
}
struct node
{
	int u,v,next;
}eg[60100];
int tot,n,a[6010],x,y,head[6010],f[6010][2],root;
bool fa[6010];
void addedge(int x,int y)
{
	eg[++tot].u=x,eg[tot].v=y,eg[tot].next=head[x],head[x]=tot;
}
void dfs(int x)
{
	f[x][0]=0;
	f[x][1]=a[x];
	for(int i=head[x]; i; i=eg[i].next)
	{
		dfs(eg[i].v);
		f[x][0]+=max(f[eg[i].v][0],f[eg[i].v][1]);
		f[x][1]+=f[eg[i].v][0];
	}
	return;
}
int main()
{
	n=read();
	for(int i=1; i<=n; ++i) a[i]=read();
	while(1)
	{
		x=read(),y=read();
		if(x==0&&y==0) break;
		addedge(y,x),fa[x]=1;
	}
	for(int i=1; i<=n; ++i) 
		if(!fa[i])
		{
			root=i;
			break;
		}
//	for(int i=1; i<n; ++i) cout<<eg[i].u<<" "<<eg[i].v<<" "<<eg[i].next<<endl;
	dfs(root);
	cout<<max(f[root][0],f[root][1])<<endl;
	return 0;
}
