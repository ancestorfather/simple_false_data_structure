#include<bits/stdc++.h>
using namespace std;
int n,k,si[20],ans,used[20],a[20];
void dfs(int v)
{
	if(v==n)
	{
		for(int i=1; i<=n; ++i)
			if(!used[i]&&abs(si[i]-a[v-1])>k) ++ans;
		return;
	}
	for(int i=1; i<=n; ++i)
		if(!used[i]&&abs(si[i]-a[v-1])>k) used[i]=1,a[v]=si[i],dfs(v+1),used[i]=0;
	return;
}
int main()
{
	cin>>n>>k,a[0]=-32767;
	for(int i=1; i<=n; ++i) cin>>si[i];
	sort(si+1,si+n+1);
	dfs(1);
	cout<<ans<<endl;
	return 0;
}
