#include<bits/stdc++.h>
using namespace std;
int n,m,a[55][55],dp[55][1300][55];
int read()
{
    int f=1,s=0;
    char ch=getchar();
    while(ch<'0'||ch>'9')
    {
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9')
    {
        s=s*10+ch-'0';
        ch=getchar();
    }
    return f*s;
}
int main()
{
	freopen("data.in","r",stdin);
	freopen("std.out","w",stdout);
    n=read(),m=read();
    for(int i=1;i<=n;i++)
        for(int j=n;j>=i;j--)
        {
            a[i][j]=read();
            a[i][j]+=a[i-1][j];
        }
    memset(dp,-0x3f,sizeof(dp));
    dp[0][0][0]=0;
    for(int i=1;i<=n;i++)
        for(int j=0;j<=min(m,(i+1)*i/2);j++)
            for(int k=0;k<=min(i,j);k++)
                for(int l=max(0,k-1);l<=i-1;l++)
                    dp[i][j][k]=max(dp[i][j][k],dp[i-1][j-k][l]+a[k][i]);
    int ans=0;
    for(int i=1;i<=n;i++)
        for(int j=0;j<=i;j++)
            ans=max(ans,dp[i][m][j]);
    cout<<ans<<endl;
    return 0;
}
