#include <bits/stdc++.h>
using namespace std;
const int maxn = 10001;
struct Edge
{
	int v,len;
	int nxt;
} e[maxn<<1]; //记录边
int head[maxn],son[maxn],cnt=0,n;
string s;
void addedge(int u,int v,int l)
{
	e[++cnt] = (Edge) {v,l, head[u]};
	head[u] = cnt;
	son[u]++;
}

void dfs(int u)
{
	cout << u << endl;
	for (int v = head[u]; v; v=e[v].nxt)
		dfs(e[v].v);
}

string convert(int u) 
{
	string s="";
	while(u)
	{
		s=char(u%10+'0') + s;
		u/=10;
	}
	return s;
}

void gyb(int u)
{
	s+=convert(u);
	if (!son[u]) return;
	s+='(';
	for (int i=0,v = head[u]; v; v=e[v].nxt,i++)
	{
		gyb(e[v].v);
		if (i+1<son[u]) s+=',';
	}
	s+=')';
}

int main() 
{
	cin >> n;
	for (int i=1;i<n;i++)
	{
		int u,v,l;
		cin >> u >> v >> l;
		addedge(u,v,l);
	}
	//dfs(1);
	s="";
	gyb(1);
	cout << s << endl;
	return 0;
}