#include<bits/stdc++.h>
using namespace std;
int x,y,m,n,l;
double d;
int main()
{
	cin>>x>>y>>m>>n>>l;
	if(m==n) cout<<"Impossible"<<endl;
	else
	{
		if(x>y) swap(x,y),swap(m,n);
		if(m>n)
		{
			d=double(y-x)/double(m-n);
			if(d!=(y-x)/(m-n)) cout<<"Impossible"<<endl;
			else cout<<d<<endl;
		}
		else
		{
			d=double(l-(y-x))/double(n-m);
			if(d!=(l-(y-x))/(n-m)) cout<<"Impossible"<<endl;
			else cout<<d<<endl;
		}
	}
	return 0;
}
