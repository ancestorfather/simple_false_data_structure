#include<bits/stdc++.h>
using namespace std;

const int maxn=1e5+10;
const pair<int,int> inf=make_pair(1e9,1e9);
int n,a,b,c,d;
vector<pair<int,int> > g[maxn];
pair<int,int> dp[maxn][2];

inline pair<int,int> operator+ (pair<int,int> a,pair<int,int> b){
	return make_pair(a.first+b.first,a.second+b.second);
}
void dfs(int pos,int fa,int type){
	pair<int,int> tmp0(0,0),tmp1(inf);
	for(int i=0,v;i<g[pos].size();++i)
		if((v=g[pos][i].first)!=fa){
			dfs(v,pos,g[pos][i].second);
			pair<int,int> nxt0,nxt1;
			nxt0=min(tmp0+dp[v][0],tmp1+dp[v][1]);
			nxt1=min(tmp1+dp[v][0],tmp0+dp[v][1]);
			tmp0=nxt0;tmp1=nxt1;
		}
	if(type==0||type==2)
		dp[pos][0]=min(tmp0,make_pair(tmp1.first+1,tmp1.second));
	else
		dp[pos][0]=inf;
	if(type==1||type==2)
		dp[pos][1]=min(make_pair(tmp0.first+1,tmp0.second+1),make_pair(tmp1.first,tmp1.second+1));
	else
		dp[pos][1]=inf;
}

int main(){
	freopen("w.in","r",stdin);
	freopen("w.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<n;++i){
		scanf("%d%d%d%d",&a,&b,&c,&d);
		if(d!=2)
			d=(c!=d);
		g[a].push_back(make_pair(b,d));
		g[b].push_back(make_pair(a,d));
	}
	dfs(1,0,0);
	printf("%d %d\n",dp[1][0].first/2,dp[1][0].second);
	return 0;
}
