#include<bits/stdc++.h>
#define N 1000001
#define ll long long
using namespace std;
int read()
{
	int f=1,s=0;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		s=s*10+ch-'0';
		ch=getchar();
	}
	return f*s;
}
ll n,m,a[N];
struct node
{
	int l,r;
	ll num,v;
}q[N*4];
void build(int p,int l,int r)
{
	q[p].l=l,q[p].r=r;
	if(l==r)
	{
		q[p].num=a[l];
		return;
	}
	int mid=(l+r)/2;
	build(p*2,l,mid);
	build(p*2+1,mid+1,r);
	q[p].num=min(q[p*2].num,q[p*2+1].num);
}
void spread(int p)
{
	if(q[p].v)
	{
		q[p*2].num-=q[p].v;
		q[p*2+1].num-=q[p].v;
		q[p*2].v+=q[p].v;
		q[p*2+1].v+=q[p].v;
		q[p].v=0;
	}
}
void change(int p,int l,int r,ll d)
{
	if(l<=q[p].l&&r>=q[p].r)
	{
		q[p].num-=d;
		q[p].v+=d;
	}
	spread(p);
	int mid=(q[p].l+q[p].r)/2;
	if(l<=mid) change(p*2,l,r,d);
	if(r>mid) change(p*2+1,l,r,d);
	q[p].num=min(q[p*2].num,q[p*2+1].num);
}
bool ask(int p,int l,int r,ll d)
{
	if(l<=q[p].l&&r>=q[p].r)
	{
		if(q[p].num<d) return false;
		else return true;
	}
	spread(p);
	int mid=(q[p].l+q[p].r)/2;
	bool val=1;
	if(l<=mid) val&=ask(p*2,l,r,d);
	if(r>mid) val&=ask(p*2+1,l,r,d);
	return val;
}
int main()
{
	n=read(),m=read();
	for(int i=1; i<=n; ++i) a[i]=read();
	build(1,1,n);
	for(int i=1; i<=m; ++i)
	{
		int x,y,z;
		z=read(),x=read(),y=read();
		if(!ask(1,x,y,z))
		{
			cout<<-1<<endl<<i<<endl;
			break;
		}
		change(1,x,y,z);
		if(i==m) cout<<0<<endl;
	}
	return 0;
}
