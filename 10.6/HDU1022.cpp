#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#define ll long long
#define INF 1000000000

using namespace std;

inline int read()
{
	register int ret=0,c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))ret=ret*10+c-'0',c=getchar();
	return ret;
}

#define M 15

int S[M],h;
int n,a[M],b[M],ans[M<<1],t,i,j,bo;
char s1[M],s2[M];

int main()
{
	while(scanf("%d %s %s",&n,s1+1,s2+1)==3)
	{
		for(int i=1;i<=n;i++)a[i]=s1[i]-'0',b[i]=s2[i]-'0';
		h=0;t=0;i=1;j=1;
		while(j<=n)
		{
			bo=1;
			while(S[h]!=b[j]&&i<=n)
				S[++h]=a[i++],ans[++t]=1;
			while(h&&S[h]==b[j])h--,j++,ans[++t]=0,bo=0;
			if(bo&&j<=n)
			{
				t=0;
				break;
			}
		}
		if(t)puts("Yes.");
		else puts("No.");
		if(t)for(int i=1;i<=t;i++)printf("%s\n",ans[i]?"in":"out");
		puts("FINISH");
	}
	return 0;
}
