#include<bits/stdc++.h>
using namespace std;
int n,m,a[55][55],v[55][55],d[55][55][510],ans;
int read()
{
    int f=1,s=0;
    char ch=getchar();
    while(ch<'0'||ch>'9')
    {
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9')
    {
        s=s*10+ch-'0';
        ch=getchar();
    }
    return f*s;
}
int dfs(int x,int y,int z)
{
	if(n+1-x-y+(n-x)*(n-x)<m-z) return 0;
    if(z==m) return a[x][y];
    if(d[x][y][z]) return d[x][y][z];
    v[x][y]=1,d[x][y][z]=a[x][y];int cnt=0;  
    for(int i=x; i<=x+1; ++i)
    {
    	int k=1;
    	if(i==x) k=y+1;
        for(int j=k; j<=n+1-i; ++j)
        {
        	if(i==1&&!v[i][j])
				cnt=max(cnt,d[x][y][z]+dfs(i,j,z+1));
            else if(i!=1&&(v[i-1][j]&&v[i-1][j+1]&&!v[i][j])) 
				cnt=max(cnt,d[x][y][z]+dfs(i,j,z+1));
        }
    }
    d[x][y][z]=cnt,v[x][y]=0;
    return d[x][y][z];
}
int main()
{
  freopen("data.in","r",stdin);
//  freopen("wh.out","w",stdout);
    n=read(),m=read();
    for(int i=1; i<=n; ++i)
        for(int j=1; j<=n+1-i; ++j)
            a[i][j]=read();
    for(int i=1; i<=n; ++i) ans=max(ans,dfs(1,i,1));
//  for(int i=m-1; i>=0; --i)
//      for(int j=1; j<=n; ++j)
//      {
//          for(int k=1; k<=n+1-j; ++k)
//              cout<<d[j][k][i]<<" ";
//          cout<<endl;
//      }
    cout<<ans<<endl;
    return 0;
}
