#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
const int maxn=1e3+10;
int n,q;
ll a[maxn][maxn],b[maxn][maxn],ans;

inline void add_a(int x,int y,int v){
	if(x<=n&&y<=n)
		a[x][y]+=v;
}
inline void add_b(int x,int y,int v){
	if(x<=n&&y<=n)
		b[x][y]+=v;
}

int main(){
	freopen("u.in","r",stdin);
	freopen("u.out","w",stdout);
	scanf("%d%d",&n,&q);
	while(q--){
		int r,c,l,s;
		scanf("%d%d%d%d",&r,&c,&l,&s);
		add_a(r,c,s);
		add_a(r+l,c+l,-s);
		add_b(r+l,c,-s);
		add_b(r+l,c+l,s);
	}
	for(int i=1;i<=n;++i)
		for(int j=1;j<=n;++j){
			if(i>1)
				a[i][j]+=a[i-1][j-1]+a[i-1][j]-a[i-2][j-1];
			else
				a[i][j]+=a[i-1][j-1]+a[i-1][j];
			b[i][j]+=b[i-1][j]+b[i][j-1]-b[i-1][j-1];
			ans^=a[i][j]+b[i][j];
		}
	printf("%lld\n",ans);
	return 0;
}
