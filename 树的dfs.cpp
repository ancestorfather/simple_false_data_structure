#include <bits/stdc++.h>
using namespace std;
const int maxn = 10001;
struct Edge
{
	int v;
	int nxt;
} e[maxn<<1]; 
int head[maxn],cnt=0,n; 

void addedge(int u,int v)
{
	e[++cnt] = (Edge) {v,head[u]};
	head[u] = cnt;
}

void dfs(int u)
{
	cout << u << endl;
	for (int v = head[u]; v; v=e[v].nxt)
		dfs(e[v].v);
}

int main() 
{
	cin >> n;
	for (int i=1;i<n;i++)
	{
		int u,v;
		cin >> u >> v;
		addedge(u,v);
	}
	dfs(1);
	return 0;
}
