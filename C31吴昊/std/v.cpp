#include<bits/stdc++.h>
using namespace std;

const int maxn=30+5;
int n,k;
char s[maxn];

namespace ${
	const int xxx=24;
	double a[1<<xxx+1];
	map<int,double> m[maxn];
	inline void init(){
		for(int i=0;i<1<<xxx+1;++i)
			a[i]=-1;
	}
	inline bool count(int bit,int len){
		if(len<=xxx)
			return a[1<<len|bit]!=-1;
		else
			return m[len].count(bit);
	}
	inline double&find(int bit,int len){
		if(len<=xxx)
			return a[1<<len|bit];
		else
			return m[len][bit];
	}
}
inline int erase(int bit,int k){
	return bit&(1<<k)-1|bit>>1&-1<<k;
}
inline double max_(double a,double b){
	return a>=b?a:b;
}
double dfs(int bit,int len){
	if(len<=k)
		return 0;
	if($::count(bit,len))
		return $::find(bit,len);
	double&res=$::find(bit,len);
	res=0;
	for(int i=0,j=len-1;i<=j;++i,--j)
		if(i<j)
			res+=max_(dfs(erase(bit,i),len-1)+(bit>>i&1),dfs(erase(bit,j),len-1)+(bit>>j&1))*2;
		else
			res+=dfs(erase(bit,i),len-1)+(bit>>i&1);
	return res/=len;
}

int main(){
	freopen("v.in","r",stdin);
	freopen("v.out","w",stdout);
	$::init();
	scanf("%d%d%s",&n,&k,s);
	k=n-k;
	int bit=0;
	for(int i=0;i<n;++i)
		bit|=(s[i]=='W')<<i;
	printf("%.10f\n",dfs(bit,n));
	return 0;
}
