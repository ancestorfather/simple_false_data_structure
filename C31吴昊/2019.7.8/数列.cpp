#include<bits/stdc++.h>
using namespace std;
int n,a[50010],len,b[5],ans;
int read()
{
	int s=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		s=s*10+ch-'0';
		ch=getchar();
	}
	return s*f;
}
void dfs(int v)
{
	for(int i=v; i<=n; ++i)
	{
		if(len==0) b[++len]=a[i],dfs(i+1),--len;
		else if(len==1&&a[i]>b[len]) b[++len]=a[i],dfs(i+1),--len;
		else if(len==2&&a[i]<b[len]) ++ans; 
	}
	return;
}
int main()
{
	n=read();
	for(int i=1; i<=n; ++i) a[i]=read();
	dfs(1);
	cout<<ans<<endl;
}
